<?php

/**
 * This is the model class for table "reporting".
 *
 * The followings are the available columns in table 'reporting':
 * @property integer $reporting_id
 * @property integer $type_user
 * @property integer $user_id
 * @property integer $count
 * @property integer $type
 * @property string $date
 */
class Reporting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reporting';
	}
        
        const CPD=0;
        const CPM=1;
        const CPA=2;
        const CPC=3;
        const CPI=4;
        
        
        

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type_user, user_id, count, type, date', 'required'),
			array('type_user, user_id, count, type', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('reporting_id, type_user, user_id, count, type, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'reporting_id' => 'Reporting',
			'type_user' => 'Type User',
			'user_id' => 'User',
			'count' => 'Count',
			'type' => 'Type',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('reporting_id',$this->reporting_id);
		$criteria->compare('type_user',$this->type_user);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('count',$this->count);
		$criteria->compare('type',$this->type);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reporting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
