<?php

/**
 * This is the model class for table "data".
 *
 * The followings are the available columns in table 'data':
 * @property integer $data_id
 * @property integer $ip
 * @property string $user_agent
 * @property integer $status
 * @property integer $user_id
 * @property integer $type_user
 */
class Data extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data';
	}
        
        const DATA_AC=1;
        const DATA_INAC=0;
        const DATA_AdUSER=1;
        const DATA_PSUSER=2;
        
        
        
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ip, user_agent, status, user_id, type_user', 'required'),
			array('ip, status, user_id, type_user', 'numerical', 'integerOnly'=>true),
			array('user_agent', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('data_id, ip, user_agent, status, user_id, type_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'data_id' => 'Data',
			'ip' => 'Ip',
			'user_agent' => 'User Agent',
			'status' => 'Status',
			'user_id' => 'User',
			'type_user' => 'Type User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('data_id',$this->data_id);
		$criteria->compare('ip',$this->ip);
		$criteria->compare('user_agent',$this->user_agent,true);
		$criteria->compare('status',Data::DATA_AC);
                
                
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type_user',$this->type_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Data the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
