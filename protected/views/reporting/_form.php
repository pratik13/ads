<?php
/* @var $this ReportingController */
/* @var $model Reporting */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reporting-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'type_user'); ?>
                <?php echo $form->dropDownList($model,'type_user',array(Data::DATA_AdUSER=>"Advertiesment User",Data::DATA_PSUSER=>"Publisher User")); ?>
		<!--<?php echo $form->textField($model,'type_user'); ?> -->
		<?php echo $form->error($model,'type_user'); ?>
	</div>
          

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
                <?php echo CHtml::activeDropDownList($model,'user_id', CHtml::listData(User::model()->findAll(), 'user_id', 'user_name'), array('empty' => '--- Select User---')); ?>          
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'count'); ?>
                
		<?php echo $form->textField($model,'count'); ?>
		<?php echo $form->error($model,'count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
                <?php echo $form->dropDownList($model,'type',array(Reporting::CPD=>"CPD",  Reporting::CPM=>"CPM" ,Reporting::CPA=>"CPA" , Reporting::CPC=>"CPC" , Reporting::CPI=>"CPI" )); ?>
		
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
            <?php 
    $this->widget('zii.widgets.jui.CJuiDatePicker',
      array(
            'attribute'=>'date',
            'model'=>$model,
            'options' => array(
                              'mode'=>'focus',
                              'dateFormat'=>'yy-m-d',
                              'showAnim' => 'slideDown',
                              ),
    'htmlOptions'=>array('size'=>30,'class'=>'date'),
          )
    );
  ?>
	
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->