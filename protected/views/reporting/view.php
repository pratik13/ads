<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('index'),
	$model->reporting_id,
);

$this->menu=array(
	array('label'=>'List Reporting', 'url'=>array('index')),
	array('label'=>'Create Reporting', 'url'=>array('create')),
	array('label'=>'Update Reporting', 'url'=>array('update', 'id'=>$model->reporting_id)),
	array('label'=>'Delete Reporting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->reporting_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Reporting', 'url'=>array('admin')),
);
?>

<h1>View Reporting #<?php echo $model->reporting_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'reporting_id',
		'type_user',
		'user_id',
		'count',
		'type',
		'date',
	),
)); ?>
