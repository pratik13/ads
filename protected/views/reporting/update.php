<?php
/* @var $this ReportingController */
/* @var $model Reporting */

$this->breadcrumbs=array(
	'Reportings'=>array('index'),
	$model->reporting_id=>array('view','id'=>$model->reporting_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Reporting', 'url'=>array('index')),
	array('label'=>'Create Reporting', 'url'=>array('create')),
	array('label'=>'View Reporting', 'url'=>array('view', 'id'=>$model->reporting_id)),
	array('label'=>'Manage Reporting', 'url'=>array('admin')),
);
?>

<h1>Update Reporting <?php echo $model->reporting_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>