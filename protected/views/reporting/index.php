<?php
/* @var $this ReportingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Reportings',
);

$this->menu=array(
	array('label'=>'Create Reporting', 'url'=>array('create')),
	array('label'=>'Manage Reporting', 'url'=>array('admin')),
);
?>

<h1>Reportings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
