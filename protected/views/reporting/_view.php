<?php
/* @var $this ReportingController */
/* @var $data Reporting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('reporting_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->reporting_id), array('view', 'id'=>$data->reporting_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_user')); ?>:</b>
	<?php echo CHtml::encode($data->type_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('count')); ?>:</b>
	<?php echo CHtml::encode($data->count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>